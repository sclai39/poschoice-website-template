﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace WebTools.Helpers
{
    public class XmlBuilder
    {

        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        public static String buildxml(String merchantid, String pwd, String txntype, String txnsource, String amount, String currency, String orderno, String cardno, String expirydate, String cvv){

            int merchantidLength = 30;
            DateTime transactiondatetime = DateTime.UtcNow;
            String id = RandomString(merchantidLength);
          

            XmlDocument doc = new XmlDocument();
            XmlElement securePayMessage = (XmlElement)doc.AppendChild(doc.CreateElement("SecurePayMessage"));
            XmlElement messageInfo = (XmlElement)securePayMessage.AppendChild(doc.CreateElement("MessageInfo"));
            XmlElement messageId = (XmlElement)messageInfo.AppendChild(doc.CreateElement("messageID"));
            messageId.SetAttribute("messageId", id);

            Console.WriteLine(transactiondatetime.ToString());
            Console.WriteLine(doc.OuterXml);

            return doc.ToString();
        }


    }
}