﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc5MobileApplicationSG.Models
{
    public class DashboardOrders
    {

        public DateTime order_dt { get; set; }
        public String cus_name { get; set; }
        public String order_type { get; set; }
        public String payment_type { get; set; }
        public String cus_phone { get; set; }
        public String address { get; set; }
        public String staked { get; set; }
        public Decimal credit { get; set; }




    }
}