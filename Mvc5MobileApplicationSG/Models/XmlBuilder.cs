﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.ComponentModel.DataAnnotations;



namespace Mvc5MobileApplicationSG.Model
{
    public class XmlBuilder 
    {

        protected XmlDocument XmlDoc;
        protected String XmlStr;



        public string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        public String buildxml(String merchantid, String pwd, String type, String source, String amount, String currency, String orderno, String cardno, String expirydate, String cvv){

            int messageidLength = 30;
            DateTime transactiondatetime = DateTime.UtcNow;
            String id = RandomString(messageidLength);
            amount = (Convert.ToDouble(amount) * 100).ToString().Trim();
            //debug
           // amount = "200";


            XmlDocument doc = new XmlDocument();
            XmlElement securePayMessage = (XmlElement)doc.AppendChild(doc.CreateElement("SecurePayMessage"));
            XmlElement messageInfo = (XmlElement)securePayMessage.AppendChild(doc.CreateElement("MessageInfo"));
            XmlElement messageID = (XmlElement)messageInfo.AppendChild(doc.CreateElement("messageID"));
            messageID.InnerText = id;
            XmlElement messageTimestamp = (XmlElement)messageInfo.AppendChild(doc.CreateElement("messageTimestamp"));
            messageTimestamp.InnerText = transactiondatetime.ToString();
            XmlElement timeoutValue = (XmlElement)messageInfo.AppendChild(doc.CreateElement("timeoutValue"));
            timeoutValue.InnerText = "60";
            XmlElement apiVersion = (XmlElement)messageInfo.AppendChild(doc.CreateElement("apiVersion"));
            apiVersion.InnerText = "xml-4.2";
            XmlElement merchantInfo = (XmlElement)securePayMessage.AppendChild(doc.CreateElement("MerchantInfo"));
            XmlElement merchantID = (XmlElement)merchantInfo.AppendChild(doc.CreateElement("merchantID"));
            merchantID.InnerText = merchantid;
            XmlElement password = (XmlElement)merchantInfo.AppendChild(doc.CreateElement("password"));
            password.InnerText = pwd;
            XmlElement requestType = (XmlElement)securePayMessage.AppendChild(doc.CreateElement("RequestType"));
            requestType.InnerText = "Payment";
            XmlElement Payment = (XmlElement)securePayMessage.AppendChild(doc.CreateElement("Payment"));
            XmlElement TxnList = (XmlElement)Payment.AppendChild(doc.CreateElement("TxnList"));
            TxnList.SetAttribute("count", "1");
            XmlElement Txn = (XmlElement)TxnList.AppendChild(doc.CreateElement("Txn"));
            Txn.SetAttribute("ID", "1");
            XmlElement txnType = (XmlElement)Txn.AppendChild(doc.CreateElement("txnType"));
            txnType.InnerText = type;
            XmlElement txnSource = (XmlElement)Txn.AppendChild(doc.CreateElement("txnSource"));
            txnSource.InnerText = source;
            XmlElement txnamount = (XmlElement)Txn.AppendChild(doc.CreateElement("amount"));
            txnamount.InnerText = amount;
            XmlElement txncurrency = (XmlElement)Txn.AppendChild(doc.CreateElement("currency"));
            txncurrency.InnerText = currency;
            XmlElement purchaseOrderNo = (XmlElement)Txn.AppendChild(doc.CreateElement("purchaseOrderNo"));
            purchaseOrderNo.InnerText = orderno;
            XmlElement CreditCardInfo = (XmlElement)Txn.AppendChild(doc.CreateElement("CreditCardInfo"));
            XmlElement txncardNumber = (XmlElement)CreditCardInfo.AppendChild(doc.CreateElement("cardNumber"));
            txncardNumber.InnerText = cardno;
            XmlElement txnexpiryDate = (XmlElement)CreditCardInfo.AppendChild(doc.CreateElement("expiryDate"));
            txnexpiryDate.InnerText = expirydate;
            XmlElement txncvv = (XmlElement)CreditCardInfo.AppendChild(doc.CreateElement("cvv"));
            txncvv.InnerText = cvv;

            XmlDoc = doc;
            XmlStr = doc.OuterXml.ToString();

            return doc.OuterXml.ToString();
        }


        protected String isApproved(String xml)
        {

            String statusapproved = "APPROVED";
            String statusnotapproved = "DECLINED";
            String statusnoresults = "NORESULTS";
            XmlDocument xmlresult = new XmlDocument();

            xmlresult.LoadXml(xml);

            if (xmlresult == null)
            {
                return statusnoresults;

            }

            XmlNode item = xmlresult.SelectSingleNode("SecurePayMessage/Payment/TxnList/Txn/approved");

            if (item == null)
            {
                return statusnotapproved;

            }

            String itemtext = item.InnerText;

            if (itemtext == "Yes")
            {
                return statusapproved;

            }
            else
            {
                return statusnotapproved;

            }


        }


    }
}