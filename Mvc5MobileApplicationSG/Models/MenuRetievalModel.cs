﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using System.Web.Mvc;
using System.Runtime.Remoting.Contexts;
using System.Net;
using System.Configuration;
using Stripe;

namespace Mvc5MobileApplicationSG.Models
{
    public class MenuRetievalModel : Model.WebServiceCallAPI
    {

        public List<CategoryModel> Categories;

        public List<ItemModel> ItemTransfer;
        public List<ItemModel> RecallItems;
        public DeliveryModel delivery;
        public String Catno = "";
        public String Grpno = "";
        public Boolean Debug = false;
        public String DiscountItemNo = "";
        public ItemModel DiscountItemModel = null;
        public string itemno { get; set; }
        public string actualitemno { get; set; }
        public double itemprice { get; set; }
        public int itemqty { get; set; }
        public DateTime orderdate { get; set; }
        public string alertmessage { get; set; }
        public double subtotal { get; set; }
        public string approved { get; set; }
        public string OrderType { get; set; }
        public string ABN { get; set; }
        public string modBtnDesc { get; set; }
        public Decimal modPrice { get; set; }
        public ModifierModel modOn { get; set; }
        public DataSet DataSetModifiers { get; set; }
        public string Notes { get; set; }
        public string CashCard { get; set; }
        public String BookingYear { get; set; }
        public String BookingMonth { get; set; }
        public String BookingDay { get; set; }
        public String Fromhh { get; set; }
        public String Frommm { get; set; }
        public String Fromtt { get; set; }
        public String Tohh { get; set; }
        public String Tomm { get; set; }
        public String Tott { get; set; }
        public String BookingTable { get; set; }
        public DataTable TablesSelect = null;
        public bool BookingCreated { get; set; }
        public bool ProgressToBookingTable { get; set; }
        public bool ProgressToBookingConfirmation { get; set; }
        public bool confirmDetails { get; set; }
        public bool noDetails { get; set; }
        public bool tarequest { get; set; }
        public bool hdrequest { get; set; }
        public bool bookingrequest { get; set; }



        public double getTotal()
        {
            double total = 0;

            for (int i = 0; i < RecallItems.Count; i++)
            {

                total = total + RecallItems[i].itemqty * RecallItems[i].itemprice;
            }

            return total;

        }

        public void notification(String message)
        {




        }

        public ItemModel CurrentItemSelected()
        {

            foreach (CategoryModel category in Categories)
            {

                foreach (GroupModel group in category.GroupModels)
                {

                    foreach (ItemModel item in group.ItemModels)
                    {
                        if (item.actualitemno == null)
                        {
                            if (item.itemno.Trim() == actualitemno)
                            {
                                return item;
                            }
                        }
                        else
                        {
                            if (item.actualitemno.Trim() == actualitemno)
                            {
                                return item;
                            }
                        }

                    }

                }


            }

            return null;

        }



        public ItemModel getItemSelected(String itemselected)
        {

            foreach (CategoryModel category in Categories)
            {

                foreach (GroupModel group in category.GroupModels)
                {

                    foreach (ItemModel item in group.ItemModels)
                    {

                        if (item.itemno.Trim() == itemselected)
                        {
                            return item;
                        }


                    }

                }


            }

            return null;

        }


        public ItemModel CurrentItemSelected(String actualitemno)
        {

            foreach (CategoryModel category in Categories)
            {

                foreach (GroupModel group in category.GroupModels)
                {

                    foreach (ItemModel item in group.ItemModels)
                    {
                        if (item.actualitemno == null)
                        {
                            if (item.itemno.Trim() == actualitemno)
                            {
                                return item;
                            }
                        }
                        else
                        {
                            if (item.actualitemno.Trim() == actualitemno)
                            {
                                return item;
                            }
                        }

                    }

                }


            }

            return null;

        }

        public string ID { get; set; }


        public string name { get; set; }


        public string phone { get; set; }

        public string phonesearch { get; set; }


        public string address { get; set; }

        public string email { get; set; }


        public string suburbname { get; set; }

        //    [Required]
        public string cardnumber { get; set; }

        //    [Required]
        public string cardexpiry { get; set; }

        //    [Required]
        public string cardcvv { get; set; }


        public string cardexpirymonth { get; set; }

        public string cardexpiryyear { get; set; }



        public MenuRetievalModel()
        {

            approved = "";
            Notes = "";
            delivery = new DeliveryModel();
            Categories = new List<CategoryModel>();
            RecallItems = new List<ItemModel>();
            ProgressToBookingConfirmation = false;
            ProgressToBookingTable = false;


        }

        public double calculateTotal()
        {

            List<ItemModel> recallitems = recallDetails();
            Double total = 0;
            foreach (ItemModel items in recallitems)
            {
                if (items.discountflag != "P")
                {
                    total = total + items.itemqty * items.itemprice;
                }

            }

            return total;

        }


        public Boolean hasFreePrawnChips()
        {

            foreach (ItemModel item in RecallItems)
            {

                if (item.itemno == "CLI524")
                {

                    return true;
                }



            }

            return false;


        }

        public Boolean isFirstItem()
        {


            foreach (ItemModel item in RecallItems)
            {

                if (item.itemno == "PROM01")
                {

                    return false;
                }



            }

            return true;


        }

        public Boolean isOnlyDiscounts()
        {

            foreach (ItemModel item in RecallItems)
            {

                if (item.discountflag == "N")
                {

                    return false;
                }



            }

            return true;

        }


        public bool firstTimeCustomer()
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();

            bool res = objRef.firsTimeOrder(ABN, orderdate);

            return res;

        }
        //      CLI1074


        public ItemModel setDiscountItemNo()
        {
            if (firstTimeCustomer())
            {
                //Creating Web Service reference object  
                ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();

                DataSet dataset = objRef.getFirstCustomerPROMO(ABN);



                ItemModel item = new ItemModel();
                item.itemno = (String)dataset.Tables[0].Rows[0]["menu_item_code"].ToString().Trim();
                item.actualitemno = (String)dataset.Tables[0].Rows[0]["menu_item_code"].ToString().Trim();
                item.itemname = (String)dataset.Tables[0].Rows[0]["menu_item_name"].ToString().Trim();
                item.itemprice = Math.Round(Convert.ToDouble(dataset.Tables[0].Rows[0]["menu_item_price"]), 2);
                item.catno = (String)dataset.Tables[0].Rows[0]["menu_item_cat"].ToString().Trim();
                double itemprice = calculateTotal() * item.itemprice;
                item.itemqty = 1;

                DiscountItemModel = item;
                //   transferItems(discountitemmodel, -1);

                if (isFirstItem())
                {
                    transferItemsPrice(item, itemprice, 1);
                }
                else if (isOnlyDiscounts())
                {
                    transferItemsPrice(item, itemprice, -1);
                }
                else
                {
                    transferItemsPrice(item, itemprice, -1);
                    transferItemsPrice(item, itemprice, 1);

                }



                return DiscountItemModel;
            }

            return null;


        }




        public IEnumerable<ValuePair> Month = new List<ValuePair> {
            new ValuePair {
                name = "January",
                value = "01"
            },
            new ValuePair {
                name = "Febuary",
                value = "02"
            },
            new ValuePair {
                name = "March",
                value = "03"
            },
            new ValuePair {
                name = "April",
                value = "04"
            },
            new ValuePair {
                name = "May",
                value = "05"
            },
            new ValuePair {
                name = "June",
                value = "06"
            },
            new ValuePair {
                name = "July",
                value = "07"
            },
            new ValuePair {
                name = "August",
                value = "08"
            },
            new ValuePair {
                name = "September",
                value = "09"
            },
            new ValuePair {
                name = "October",
                value = "10"
            },
            new ValuePair {
                name = "November",
                value = "11"
            },
            new ValuePair {
                name = "December",
                value = "12"
            }
        };

        public IEnumerable<ValuePair> Year = new List<ValuePair> {
            new ValuePair {
                name = "2020",
                value = "2020"
            },
            new ValuePair {
                name = "2021",
                value = "2021"
            },
            new ValuePair {
                name = "2022",
                value = "2022"
            },
            new ValuePair {
                name = "2023",
                value = "2023"
            },
            new ValuePair {
                name = "2024",
                value = "2024"
            },
            new ValuePair {
                name = "2025",
                value = "2025"
            },
            new ValuePair {
                name = "2026",
                value = "2026"
            },
            new ValuePair {
                name = "2027",
                value = "2027"
            },
            new ValuePair {
                name = "2028",
                value = "2028"
            },
            new ValuePair {
                name = "2029",
                value = "2029"
            },
            new ValuePair {
                name = "2030",
                value = "2030"
            }
        };

        public IEnumerable<SelectListItem> CashCardList
        {
            get
            {
                return new SelectList(CashCard, "value", "value");

            }
        }

        public IEnumerable<SelectListItem> MonthList
        {
            get
            {
                return new SelectList(Month, "value", "name");

            }
        }

        public IEnumerable<SelectListItem> YearList
        {
            get
            {
                return new SelectList(Year, "value", "value");

            }
        }



        public IEnumerable<SelectListItem> TablesItems
        {
            get
            {

                DataTable dt = taken();

                var data = dt.AsEnumerable().Select(row =>

                            new
                            {
                                TableNo = row["TableNo"].ToString(),
                                size = row["size"].ToString(),
                                title = row["title"].ToString()
                            });


                var data2 = dt.AsEnumerable().Select(row =>

                            new
                            {
                                TableNo = row["TableNo"].ToString(),
                                size = row["size"].ToString(),
                                title = row["title"].ToString()
                            });


                var data3 = data2.ToList();

                data3.Clear();


               

                if (dt != null || dt.Rows != null)
                {
                   
                    foreach(var item in data)
                    {
                        foreach(var item2 in data)
                        {

                            if(item.TableNo.Trim() == item2.TableNo.Trim())
                            {
                                continue;

                            }else
                            {
                                bool exists = false;
                                foreach (var item3 in data3)
                                {
                                    if ((item3.TableNo.Trim() == item.TableNo.Trim()) || (item3.size.Trim() == item.size.Trim()))
                                    {
                                        exists = true;
                                        break;

                                    }

                                }

                                if (!exists)
                                {
                                    data3.Add(item);

                                }
                            }

                        }
                    }
                    return new SelectList(data3.AsEnumerable().Select(n => n).OrderBy(x => x.size), "TableNo", "size");

                }
                else
                {
                    return null;
                }
            }
        }


        public IEnumerable<SelectListItem> TransferItems
        {
            get
            {

                getDeliverySuburbs();
                if (ItemTransfer != null)
                {
                    var selectlist = new SelectList(ItemTransfer, "itemno", "itemname");
                    foreach (var item in selectlist)
                    {
                        if (item.Value == itemno)
                        {
                            item.Selected = true;

                        }

                    }

                    return selectlist;

                }
                else
                {
                    return null;
                }
            }
        }



        public List<CategoryModel> getCategories()
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            DataSet dataset = objRef.getCategory(ABN);

            DataSet datasetModifiers = objRef.getAllModifiers(ABN);
            DataSetModifiers = datasetModifiers;
            List<CategoryModel> categoryitems = new List<CategoryModel>();

            for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
            {
                CategoryModel item = new CategoryModel(ABN, datasetModifiers);
                item.catno = (String)dataset.Tables[0].Rows[i]["cat_no"].ToString().Trim();
                item.description = (String)dataset.Tables[0].Rows[i]["btn_desc"].ToString().Trim();
                item.getGroups();
                //  item.Modifiers = datasetModifiers;
                categoryitems.Add(item);
            }


            Categories = categoryitems;


            return categoryitems;



        }

        public ItemModel buildItem(String itemno, String itemname, Double itemprice, String itemcat, String itemgroup)
        {
            ItemModel item = new ItemModel();
            item.itemno = itemno;
            item.actualitemno = itemno;
            item.catno = itemcat;
            item.itemprice = itemprice;
            item.grpno = itemgroup;

            return item;


        }


        public List<ItemModel> getDeliverySuburbs()
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            DataSet dataset = objRef.getAllDeliveryItems(ABN);

            List<ItemModel> items = new List<ItemModel>();

            ItemModel itemmodel = new ItemModel();

            items.Add(itemmodel);


            for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
            {
                ItemModel item = new ItemModel();
                item.itemno = (String)dataset.Tables[0].Rows[i]["menu_item_code"].ToString().Trim();
                item.itemname = (String)dataset.Tables[0].Rows[i]["menu_item_name"].ToString().Trim();
                item.itemprice = Math.Round(Convert.ToDouble(dataset.Tables[0].Rows[i]["menu_item_price"]), 2);
                item.description = (String)dataset.Tables[0].Rows[i]["description"].ToString().Trim();

                items.Add(item);
            }

            ItemTransfer = items;

            return ItemTransfer;


        }

        public ItemModel findItem()
        {

            foreach (ItemModel item in ItemTransfer)
            {
                if (item == null)
                {
                    continue;
                }

                if (item.itemno == itemno)
                {
                    return item;
                }


            }

            return null;

        }


        public DataTable taken()
        {

            DataTable result = new DataTable();

            //  Fromhh = 0;
            /*   String fromhh = Fromhh;
               if (Fromtt == "PM")
               {
                   fromhh = (Convert.ToInt32(Fromhh) + 12).ToString();
               }



               DateTime from = new DateTime(Convert.ToInt32(BookingYear), Convert.ToInt32(BookingMonth), Convert.ToInt32(BookingDay), Convert.ToInt32(fromhh), Convert.ToInt32(Frommm), 0);

               String tohh = Tohh;
               if (Tott == "PM")
               {
                   tohh = (Convert.ToInt32(Tohh) + 12).ToString();
               }


               DateTime to = new DateTime(Convert.ToInt32(BookingYear), Convert.ToInt32(BookingMonth), Convert.ToInt32(BookingDay), Convert.ToInt32(tohh), Convert.ToInt32(Tomm), 0);

               */

            try
            {
                //Creating Web Service reference object  
                ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                result = objRef.bindTables(ABN, BookingYear, BookingMonth, BookingDay, Fromhh, Frommm, Fromtt, Tohh, Tomm, Tott);

                TablesSelect = result;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


            return result;

        }

        public DateTime createBooking(DateTime from, DateTime to)
        {

            DateTime result = DateTime.Now;

            address = "";

            try
            {
                //Creating Web Service reference object  
                ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                result = objRef.createheaderWithDate(ABN, name, "N", Notes, "booking", "", phone, address, BookingTable, "N", from, to, "CLOUD", "N", "N", email);

                orderdate = result;

                BookingCreated = true;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


            return result;

        }



        public DateTime createOrder(String ordertype)
        {

            getDeliverySuburbs();
            ItemModel suburb = findItem();
            DateTime result = DateTime.Now;
            OrderType = ordertype;

            try
            {
                //Creating Web Service reference object  
                ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                result = objRef.createheader(ABN, name, "N", Notes, ordertype, "", phone, address, "", "N");
                String catno = objRef.getdeliverycatno(ABN);

                String res = objRef.transferItem(ABN, suburb.itemname, suburb.itemno, suburb.itemprice.ToString(), 1, catno, result);

                orderdate = result;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            confirmDetails = false;

            return result;
        }

        public DateTime createOrderTA(String ordertype)
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            DateTime result = objRef.createheader(ABN, name, "N", "", ordertype, "", phone, "", "", "N");
            OrderType = ordertype;
            orderdate = result;

            confirmDetails = false;

            return result;
        }

        public DateTime createOrderWithEmail(String ordertype)
        {

            getDeliverySuburbs();
            ItemModel suburb = findItem();
            DateTime result = DateTime.Now;
            OrderType = ordertype;

            try
            {
                //Creating Web Service reference object  
                ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                result = objRef.createheaderWithEmail(ABN, name, "N", "", ordertype, "", phone, address, "", "N", email);
                String catno = objRef.getdeliverycatno(ABN);

                String res = objRef.transferItem(ABN, suburb.itemname, suburb.itemno, suburb.itemprice.ToString(), 1, catno, result);

                orderdate = result;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            confirmDetails = false;

            return result;
        }

        public DateTime createOrderTAWithEmail(String ordertype)
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            DateTime result = objRef.createheaderWithEmail(ABN, name, "N", "", ordertype, "", phone, "", "", "N", email);
            OrderType = ordertype;
            orderdate = result;

            confirmDetails = false;

            return result;
        }

        public String transferItemsPrice(ItemModel item, double price, int signal)
        {
            //CurrentItemSelected = item;
            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            String res = objRef.transferItemSimplePrice(ABN, orderdate, item.itemno, price, signal);

            return res;

        }

        public String transferItems(ItemModel item, int signal)
        {
            //CurrentItemSelected = item;
            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            String res = objRef.transferItemSimple02(ABN, orderdate, item.itemno, signal);

            return res;

        }

        public void forceModify(String BtnDesc, Decimal price)
        {
            //CurrentItemSelected = item;
            //Creating Web Service reference object  

            ItemModel itemModel = CurrentItemSelected();

            itemModel.removedModifiers.Add(BtnDesc);

            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            objRef.createSpecial(ABN, orderdate, itemno, BtnDesc, Convert.ToDecimal(price), 1, OrderType);


        }

        public ModifierModel GetNameOfModifierOn()
        {

            foreach (DataRow row in DataSetModifiers.Tables[0].Rows)
            {
                String menu_item_code = row["mnu_itm_code"].ToString().Trim();


                String modcode = row["modcode"].ToString().Trim();

                decimal price = 0;

                try
                {
                    price = Convert.ToDecimal(row["price"]);

                }
                catch (Exception ex)
                {
                    price = 0;
                }

                String btn_desc = row["btn_desc"].ToString().Trim();
                // String description = row["description"].ToString().Trim();

                String forced = "N";

                try
                {
                    // if (!(row["forced"] is DBNull))
                    // {
                    forced = row["forced"].ToString().Trim();

                    //  }
                }
                catch (Exception ex)
                {
                    forced = "N";

                }

                ModifierModel model = new ModifierModel();
                model.MenuItemNo = menu_item_code;
                //model.price = price;
                model.price = price; //debug
                model.BtnDesc = btn_desc;
                //    model.description = description;
                model.forced = forced;

                if (model.BtnDesc.Trim() == modBtnDesc.Trim())
                {
                    model.On = true;
                    modOn = model;
                    return model;

                }


            }
            return null;

        }

        public List<ItemModel> recallDetails()
        {
            if (name == null || name.Trim() == "")
            {
                return new List<ItemModel>();
            }

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            DataSet dataset = objRef.RecallOrderDetails(ABN, orderdate);
            List<ItemModel> items = new List<ItemModel>();
            for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
            {
                ItemModel item = new ItemModel();
                item.itemno = (String)dataset.Tables[0].Rows[i]["order_item_no"].ToString().Trim();
                item.itemname = (String)dataset.Tables[0].Rows[i]["order_item_name"].ToString().Trim();
                item.itemprice = Math.Round(Convert.ToDouble(dataset.Tables[0].Rows[i]["order_item_price"]), 2);
                item.itemqty = Convert.ToInt32(dataset.Tables[0].Rows[i]["order_qty"]);
                item.special = (String)dataset.Tables[0].Rows[i]["special"].ToString().Trim();

                if (dataset.Tables[0].Rows[i]["menu_item_code"] == null)
                {
                    item.actualitemno = "";
                }
                else
                {
                    item.actualitemno = (String)dataset.Tables[0].Rows[i]["menu_item_code"].ToString().Trim();
                }


                if (dataset.Tables[0].Rows[i]["discount_flag"] == null)
                {
                    item.discountflag = "N";
                }
                else
                {
                    item.discountflag = (String)dataset.Tables[0].Rows[i]["discount_flag"].ToString().Trim();

                }

                items.Add(item);

            }

            RecallItems = items;

            return items;

        }


        public PaymentIntent payIntent(int amt)
        {

            // Set your secret key. Remember to switch to your live secret key in production!
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            if (ABN == "66166037912")
            {
                StripeConfiguration.ApiKey = "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";

            }
            else if (ABN == "42150338373")
            {
                StripeConfiguration.ApiKey = "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";

            }

            //   StripeConfiguration.ApiKey = "sk_test_51GuFRFGSzY6XVzcsQkoYQdx219OC9vof0JwFOPVlrXhi4AlcRr2Ycf0HORTmVdlvCJx2IjBJJTPts7CVyg6f0deU00KK7x4icf";

            var options = new PaymentIntentCreateOptions
            {
                Amount = amt,
                Currency = "aud",
                // Verify your integration in this guide by including this parameter
                Metadata = new Dictionary<string, string>
                {
                  { "integration_check", "accept_a_payment" },
                },
            };

            var service = new PaymentIntentService();
            var paymentIntent = service.Create(options);


            return paymentIntent;


        }


        public PaymentIntent payIntentFee(int amt, String acctid)
        {

            // Set your secret key. Remember to switch to your live secret key in production!
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            StripeConfiguration.ApiKey = "sk_live_swVbCYRStKrgXpOVqYYEK64N00wgg4U4nr";
            //   StripeConfiguration.ApiKey = "sk_test_51GuFRFGSzY6XVzcsQkoYQdx219OC9vof0JwFOPVlrXhi4AlcRr2Ycf0HORTmVdlvCJx2IjBJJTPts7CVyg6f0deU00KK7x4icf";
            double cost = amt / 100;
            double commission = cost * 0.05;
            double stripefee = cost * 0.0175 + 0.30;
            double fee = (commission - stripefee) * 100;

            var options = new PaymentIntentCreateOptions
            {
                PaymentMethodTypes = new List<string>
                {
                  "card",
                },
                Amount = amt,
                Currency = "aud",
                ApplicationFeeAmount = (long)fee,
                // Verify your integration in this guide by including this parameter
                // Metadata = new Dictionary<string, string>
                // {
                //   { "integration_check", "accept_a_payment" },
                // },
            };

            var service = new PaymentIntentService();
            var requestOptions = new RequestOptions();
            requestOptions.StripeAccount = acctid;

            var paymentIntent = service.Create(options, requestOptions);


            return paymentIntent;


        }




        public void pay(String amount)
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            objRef.paidCredit(ABN, orderdate, Convert.ToDecimal(amount), Convert.ToDecimal(amount));



        }

        public void payWithReady(String amount, DateTime ready)
        {


            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            objRef.paidCreditWithTime(ABN, orderdate, Convert.ToDecimal(amount), Convert.ToDecimal(amount), ready);



        }

        public String payment()
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            return objRef.pay(ABN, orderdate, TransactionType, TransactionSource, subtotal.ToString(), TransactionCurrency, cardnumber, cardexpiry, cardcvv);


        }

        public bool getDeliveryOn()
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            bool deliveryOn = objRef.checkDeliveryOn(ABN);

            return deliveryOn;

        }

        public bool getTakeawayOn()
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            bool takeawayOn = objRef.checkTakeawayOn(ABN);

            return takeawayOn;

        }

        public bool getOffOnline()
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            bool offonline = objRef.checkOffonline(ABN);

            return offonline;

        }

        public void updateContact()
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            objRef.updateEmailOrderHdr(ABN, orderdate, email, phone, Notes);


        }

        public void confirmedcontact()
        {

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            objRef.confirmOrderHdr(ABN, orderdate);


        }

        public void findcontactTA(String pcusphone, String ordertype)
        {


            confirmDetails = true;
            itemno = " ";

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            DataTable dt = objRef.searchContactTA(ABN, pcusphone, ordertype);

            if (dt.Rows.Count == 0)
            {
                dt = objRef.searchContact(ABN, pcusphone, "homedelivery");

            }

            if (dt.Rows.Count > 0)
            {


                noDetails = false;

                name = (String)dt.Rows[0]["cus_name"];
                phone = (String)dt.Rows[0]["cus_phone"];


                try
                {
                    email = (String)dt.Rows[0]["email"];

                }
                catch (Exception ex)
                {
                    email = "";
                    name = "";
                    phone = "";
                    
                }




            }
            else
            {

                noDetails = true;

            }




        }


        public void findcontact(String pcusphone, String ordertype)
        {


            confirmDetails = true;
            itemno = " ";

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            DataTable dt = objRef.searchContact(ABN, pcusphone, ordertype);

            if (dt.Rows.Count == 0)
            {
                dt = objRef.searchContact(ABN, pcusphone, "homedelivery");

            }

            if (dt.Rows.Count > 0)
            {
                if (ordertype == "homedelivery")
                {
                    try
                    {
                        address = (String)dt.Rows[0]["address"];
                        itemno = ((String)dt.Rows[0]["menu_item_code"]).Trim();
                        if (address.Trim() == "")
                        {

                            noDetails = true;

                            phone = " ";
                            suburbname = " ";
                            return;


                        }
                    }
                    catch (Exception ex)
                    {
                        address = " ";
                        itemno = "";
                        noDetails = true;

                        phone = " ";
                        suburbname = " ";
                        return;
                    }

                }


                noDetails = false;

                name = (String)dt.Rows[0]["cus_name"];
                phone = (String)dt.Rows[0]["cus_phone"];


                try
                {
                    email = (String)dt.Rows[0]["email"];

                }
                catch (Exception ex)
                {
                    email = "";
                    name = "";
                    phone = "";
                    itemno = "";

                }




            }
            else
            {

                noDetails = true;

            }




        }

        public void findcontactBooking(String pcusphone)
        {


            confirmDetails = true;
            itemno = " ";

            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            DataTable dt = objRef.searchContactBooking(ABN, pcusphone);
           

            if (dt.Rows.Count > 0)
            {


                noDetails = false;

                name = (String)dt.Rows[0]["cus_name"];
                phone = (String)dt.Rows[0]["cus_phone"];


                try
                {
                    email = (String)dt.Rows[0]["email"];

                }
                catch (Exception ex)
                {
                    email = "";
                    name = "";
                    phone = "";

                }




            }
            else
            {

                noDetails = true;

            }




        }

        public String generateID()
        {

            Random rnd = new Random();
            String id = "";

            for (int i = 0; i < 4; i++)
            {
                id = id + rnd.Next(0, 9).ToString();

            }

            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            String res = objRef.sendemailText(ABN, orderdate, email, "Generated ID: " + id, "Your generated ID is " + id);

            ID = id;

            return id;
        }

    }
}