﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using System.Web.Mvc;
using System.Runtime.Remoting.Contexts;
using System.Configuration;
using System.Net;

namespace Mvc5MobileApplicationSG.Models
{
    public class CategoryModel
    {

        public List<GroupModel> GroupModels { get; set; }

        public string catno { get; set; }

        public string btndesc { get; set; }

        public string description { get; set; }

        public string ABN { get; set; }

        public DataSet Modifiers { get; set; }


        public CategoryModel(string abn)
        {
            ABN = abn;
           
        }

        public CategoryModel(string abn, DataSet modifierset)
        {
            ABN = abn;
            Modifiers = modifierset;

        }

        public List<GroupModel> getGroups()
        {
           
            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            
            DataSet dataset = objRef.getGroup(ABN, catno);

            List<GroupModel> groupitems = new List<GroupModel>();

            for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
            {
                GroupModel item = new GroupModel(ABN, Modifiers);
                item.grpno = (String)dataset.Tables[0].Rows[i]["grp_no"].ToString().Trim();
                item.description = (String)dataset.Tables[0].Rows[i]["btn_desc"].ToString().Trim();
                item.Modifiers = Modifiers;
                item.getItems(item.grpno);
                

                groupitems.Add(item);
            }


            GroupModels = groupitems;
           
            return GroupModels;


        }

    }
}