﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;




namespace Mvc5MobileApplicationSG.Model
{
    public class WebServiceCallAPI : XmlBuilder 
    {

      //  public static string DestinationTestURL = "https://test.api.securepay.com.au/xmlapi/payment";
      //  public static string TestMerchantAccount = "ABC0001";
      //  public static string TestMerchantPwd = "abc123";

        public static string DestinationTestURL = "https://api.securepay.com.au/xmlapi/payment";
        public static string TestMerchantAccount = "4TN0031";
        public static string TestMerchantPwd = "Klfi7xdc";


        public static string TransactionType ="0";
        public static string TransactionSource = "23";
        public static string TransactionCurrency = "AUD";




        public string postXMLData(string requestXml)
        {
            // using System.Net;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            // Use SecurityProtocolType.Ssl3 if needed for compatibility reasons

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(DestinationTestURL);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }



    }
}