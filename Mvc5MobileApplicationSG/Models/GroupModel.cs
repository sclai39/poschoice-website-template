﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using System.Web.Mvc;
using System.Runtime.Remoting.Contexts;
using System.Diagnostics;
using System.Configuration;
using System.Net;

namespace Mvc5MobileApplicationSG.Models
{
    public class GroupModel
    {

        public List<ItemModel> ItemModels { get; set; }

        public string grpno { get; set; }

        public string description { get; set; }

        public string ABN { get; set; }

        public DataSet Modifiers { get; set; }


        public string getinputboxstr(int i)
        {
            return "inputbox" + ItemModels[i].itemno;

        }

        public string getaddbuttonstr(int i)
        {
            return "addbutton" + ItemModels[i].itemno;

        }


        public string getdeductbuttonstr(int i)
        {
            return "deductbutton" + ItemModels[i].itemno;

        }



        public List<ItemModel> ItemsAll;


        public GroupModel(string abn, DataSet modifierset)
        {
            ABN = abn;
            ItemModels = new List<ItemModel>();
            getAllItems(modifierset);          

        }

        public List<ItemModel> getItems(String grpno)
        {
            ItemModels.Clear();
            foreach (ItemModel item in ItemsAll)
            {

                if (item.grpno.Trim() == grpno.Trim())
                {

                    ItemModels.Add(item);

                }


            }

            return ItemModels;

        }



        public List<ItemModel> getAllItems(DataSet modifierset)
        {

            
            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            
            DataSet dataset = objRef.getAllItems(ABN);

            List<ItemModel> items = new List<ItemModel>();

            for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
            {
                ItemModel item = new ItemModel();
                item.itemno = (String)dataset.Tables[0].Rows[i]["menu_item_code"].ToString().Trim();
                item.actualitemno = (String)dataset.Tables[0].Rows[i]["menu_item_code"].ToString().Trim();
                item.itemname = (String)dataset.Tables[0].Rows[i]["menu_item_name"].ToString().Trim();
                item.itemprice = Math.Round(Convert.ToDouble(dataset.Tables[0].Rows[i]["menu_item_price"]), 2);
                item.description = (String)dataset.Tables[0].Rows[i]["description"].ToString().Trim();
                item.dietry = (String)dataset.Tables[0].Rows[i]["dietry"].ToString().Trim();
                item.grpno = (String)dataset.Tables[0].Rows[i]["menu_item_group"].ToString().Trim();
                item.stocklvl = Math.Round(Convert.ToDouble(dataset.Tables[0].Rows[i]["stocklvl"]), 2);
                item.nostock = (String)dataset.Tables[0].Rows[i]["nostock"];
             //   item.Modifiers = modifierset;
                item.buildModifierModels(modifierset);

                if (item.description.Trim() == "")
                {
                    //do nothing
                }
                else if (item.description.Trim().IndexOf("\n") < 0)
                {
                    item.itemlongname = item.description.ToUpper();

                }
                else
                {
                    item.itemlongname = item.description.Substring(0, item.description.IndexOf("\n")).ToUpper();

                }

                items.Add(item);
            }

            ItemsAll = items;

            return ItemsAll;


        }


    }
}