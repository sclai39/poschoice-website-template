﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;




namespace Mvc5MobileApplicationSG.Model
{
    public class WebServiceCallAPI
    {

        public static string DestinationTestURL = "https://test.api.securepay.com.au/xmlapi/payment";


        public string postXMLData(string requestXml)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(DestinationTestURL);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                response.Close();
                return responseStr;
            }
            response.Close();
            return null;
        }



    }
}