﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mvc5MobileApplicationSG.Models
{
    public class AdminPanelModel
    {

        public String User { get; set; }
        public String Pwd { get; set; }
        public String ABN { get; set; }
        public Boolean on { get; set; }
        public List<CategoryModel> categories;
        public CategoryModel currentCategory;
        public List<DashboardOrders> Dashboardorders;

        public IEnumerable<ValuePair> Business = new List<ValuePair> {
            new ValuePair {
                name = "bambusa",
                value = "42150338373"
            },
            new ValuePair {
                name = "bbqnation",
                value = "66166037912"
            },
            new ValuePair {
                name = "springgarden",
                value = "67645919268"
            },
            new ValuePair {
                name = "testdb",
                value = "testdb"
            },
            new ValuePair {
                name = "garyskitchen",
                value = "98840579013"
            },
            new ValuePair {
                name = "chomolungma",
                value = "86602312069"
            },
            new ValuePair {
                name = "lavitadolce",
                value = "65483704728"
            }

        };


        public AdminPanelModel()
        {
            ABN = "testdb";
        }

        public IEnumerable<SelectListItem> businessList
        {
            get
            {
                return new SelectList(Business, "value", "name");

            }
        }

        public void turnOnServices(String abn, Boolean on)
        {
            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //  var request = (HttpWebRequest)WebRequest.Create(objRef.Endpoint.ListenUri);
            //  request.GetResponse();
            // objRef.Endpoint.Address = new System.ServiceModel.EndpointAddress(request.Address);

            if (on) {
               objRef.UpdateDeliveryOn(abn, "Y");
               objRef.UpdateTakeawayOn(abn, "Y");

            }
            else
            {
                objRef.UpdateDeliveryOn(abn, "N");
                objRef.UpdateTakeawayOn(abn, "N");

            }


        }


        public bool login()
        {
            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //  var request = (HttpWebRequest)WebRequest.Create(objRef.Endpoint.ListenUri);
            //  request.GetResponse();
            // objRef.Endpoint.Address = new System.ServiceModel.EndpointAddress(request.Address);

            bool res = objRef.login("ALL", User, Pwd);

            return res;


        }


        public void getDashboardOrders()
        {

            Dashboardorders = new List<DashboardOrders>();

            DateTime today = DateTime.Today;
            //Creating Web Service reference object  
            ServiceReference_poschoice_ws_api.WebServiceMainSoapClient objRef = new ServiceReference_poschoice_ws_api.WebServiceMainSoapClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //  var request = (HttpWebRequest)WebRequest.Create(objRef.Endpoint.ListenUri);
            //  request.GetResponse();
            // objRef.Endpoint.Address = new System.ServiceModel.EndpointAddress(request.Address);

            DataSet dataset = objRef.getOrders(ABN, today);

            for(int i = 0; i < dataset.Tables[0].Rows.Count; i++)
            {

                DashboardOrders item = new DashboardOrders();
                item.order_dt = (DateTime)dataset.Tables[0].Rows[i]["order_dt"];
                item.order_type = (String)dataset.Tables[0].Rows[i]["order_type"].ToString().Trim();
                item.payment_type = (String)dataset.Tables[0].Rows[i]["payment_type"].ToString().Trim();
                item.staked = (String)dataset.Tables[0].Rows[i]["staked"].ToString().Trim();
                item.cus_name = (String)dataset.Tables[0].Rows[i]["cus_name"].ToString().Trim();
                item.cus_phone = (String)dataset.Tables[0].Rows[i]["cus_phone"].ToString().Trim();
                item.address = (String)dataset.Tables[0].Rows[i]["address"].ToString().Trim();
                item.credit = Math.Round(Convert.ToDecimal(dataset.Tables[0].Rows[i]["credit"]), 2);
               
                Dashboardorders.Add(item);


            }


        }




    }
}