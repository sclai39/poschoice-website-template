﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc5MobileApplicationSG.Models
{
    public class DeliveryModel
    {

        public string name { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string suburbname { get; set; }
        public string suburbsurccode { get; set; }
        public DateTime orderdate { get; set; }


    }
}