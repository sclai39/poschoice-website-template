﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc5MobileApplicationSG.Models
{
    public class ModifierModel
    {

        public string MenuItemNo { get; set; }

        public string ModCode { get; set; }

        public string BtnDesc { get; set; }

        public decimal price { get; set; }

        public string description { get; set; }

        public string forced { get; set; }

        public bool On { get; set; }



    }
}