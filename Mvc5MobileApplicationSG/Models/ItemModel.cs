﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mvc5MobileApplicationSG.Models
{
    public class ItemModel
    {
        public ItemModel()
        {

            itemqty = 0;
        }

        public string actualitemno { get; set; }

        public string itemno { get; set; }

        public string itemname { get; set; }

        public string special { get; set; }

        public string description { get; set; }

        public string dietry { get; set; }

        public double itemprice { get; set; }

        public int itemqty { get; set; }

        public string catno { get; set; }

        public string grpno { get; set; }

        public string discountflag { get; set; }

        public double stocklvl { get; set; }

        public string nostock { get; set; }

        public DataSet Modifiers { get; set; }

        public List<String> removedModifiers = new List<String>();

        public List<ModifierModel> Modifiermodels { get; set; }



        public bool isSpecial()
        {

            if(itemno.Trim() == special.Trim())
            {
                return false;

            }
            else
            {
                return true;

            }

        }

        public IEnumerable<SelectListItem> getForcedModifiers()
        {
            List<ModifierModel> forced = new List<ModifierModel>();
            foreach (ModifierModel modifier in Modifiermodels)
            {
                if (modifier.forced == "Y")
                {
                    forced.Add(modifier);
                }


            }
            return new SelectList(forced, "BtnDesc", "BtnDesc");

        }

        public IEnumerable<SelectListItem> getNonForcedModifiers()
        {
            List<ModifierModel> notforced = new List<ModifierModel>();
            foreach(ModifierModel modifier in Modifiermodels)
            {
                if(modifier.forced == "N" && !removedModifiers.Contains(modifier.BtnDesc))
                {
                    notforced.Add(modifier);
                }


            }
            return new SelectList(notforced, "BtnDesc", "BtnDesc");
              
        }


        public void buildModifierModels(DataSet modifierset)
        {

            Modifiermodels = new List<ModifierModel>();
            foreach (DataRow row in modifierset.Tables[0].Rows)
            {


                String menu_item_code = row["mnu_itm_code"].ToString().Trim();

                if (menu_item_code == itemno)
                {
                    String modcode = row["modcode"].ToString().Trim();

                    decimal price = 0;
                    if (!(row["price"] is DBNull))
                    {
                        price = Convert.ToDecimal(row["price"]);
                    }
                    
                    String btn_desc = row["btn_desc"].ToString().Trim();
                   // String description = row["description"].ToString().Trim();

                    String forced = "N";
                    if (!(row["forced"] is DBNull))
                    {
                        forced = row["forced"].ToString().Trim();
                    }
                    

                    ModifierModel model = new ModifierModel();
                    model.MenuItemNo = menu_item_code;
                    model.price = price;
                    model.BtnDesc = btn_desc;
                //    model.description = description;
                    model.forced = forced;

                    addModel(model);


                }



            }
        }


        public void addModel(ModifierModel modifiermodel)
        {

            foreach(ModifierModel model in Modifiermodels)
            {

                if(model.BtnDesc.Trim() == modifiermodel.BtnDesc.Trim())
                {

                    return;
                }

            }

            Modifiermodels.Add(modifiermodel);


        }

        public double minusStockLvl() 
        {
            double currentstock = stocklvl;
            
            if(currentstock == 0)
            {
                return currentstock;
            }

            stocklvl = stocklvl - 1;

            return currentstock;
        
        }


        public double addStockLvl()
        {

            double currentstock = stocklvl;
            stocklvl = stocklvl + 1;
            return currentstock;


        }





        public string itemlongname { get; set; }

        public string getinputboxstr()
        {

            if (actualitemno == null)
            {
                return "inputbox" + itemno;

            }
            else
            {
                return "inputbox" + actualitemno;
            }

        }

        public string getpaymentinputboxstr()
        {
              return "inputbox" + itemno;

        }

        public string getcheckoutstr()
        {
            return "checkout" + itemno;

        }

        public string getaddbuttonstr()
        {
            return "addbutton" + actualitemno;

        }


        public string getdeductbuttonstr()
        {
            return "deductbutton" + actualitemno;

        }




    }
}