﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc5MobileApplicationSG.Models;

namespace Mvc5MobileApplicationSG.Controllers
{
    public class POSAppAdminPanelController : Controller
    {

        public static string ABN = "ALL";


        public ActionResult mainPage()
        {

          //  return View("blank");

            
            AdminPanelModel adminmodel = new AdminPanelModel();
            adminmodel.ABN = ABN;
            Session["adminmodel"] = adminmodel;
            return View("adminLogin", adminmodel);
           
            

        }


        public ActionResult turnOnService(AdminPanelModel adminmodel)
        {

            adminmodel.turnOnServices(adminmodel.ABN, adminmodel.on);
            return View("services", adminmodel);

        }


        public ActionResult dashboardOrders(AdminPanelModel adminmodel)
        {

            adminmodel.getDashboardOrders();
            return View("dashboardOrders", adminmodel);

        }


        [HttpPost]
        public ActionResult login(AdminPanelModel adminmodel)
        {
            //AdminPanelModel admin = (AdminPanelModel)Session["adminmodel"];
            bool res = adminmodel.login();
            if (res)
            {
                return View("selection");
            }
            else
            {
                return View("adminLogin", adminmodel);

            }

        }

        [HttpPost]
        public ActionResult logout(AdminPanelModel adminmodel)
        {
            //AdminPanelModel admin = (AdminPanelModel)Session["adminmodel"];
            /*   bool res = adminmodel.login();
               if (res)
               {
                   return View("selection");
               }
               else
               {
                   return View("adminLogin", adminmodel);

               } */

            return null;

        }

        public ActionResult getServices(AdminPanelModel adminmodel)
        {

            return View("services", adminmodel);


        }

        public ActionResult getItemsFrame(AdminPanelModel adminmodel)
        {

            return View("itemsFrame");


        }

        public ActionResult getModifiersFrame(AdminPanelModel adminmodel)
        {

            return View("modifiersFrame");


        }

        public ActionResult modifiersPage(AdminPanelModel adminmodel)
        {

            return View("modifiersPage", adminmodel);

        }

        public ActionResult categoryPage(String catno, string btndesc, string description)
        {
            AdminPanelModel adminmodel = new AdminPanelModel();
          
            adminmodel.currentCategory = new CategoryModel(ABN);
            adminmodel.currentCategory.catno = catno;
            adminmodel.currentCategory.btndesc = btndesc;
            adminmodel.currentCategory.description = description;


            return View("categoryPage", adminmodel);


        }

        public ActionResult getCategoryFrame()
        {
            AdminPanelModel adminmodel = new AdminPanelModel();
         //   adminmodel.abn = ABN;
         //   adminmodel.getCategoryModel();
            return View("categoryFrame", adminmodel);


        }

        public ActionResult groupPage(AdminPanelModel adminmodel)
        {

            return View("groupPage", adminmodel);

        }

        public ActionResult getGroupFrame(AdminPanelModel adminmodel)
        {

            return View("groupFrame");


        }

        public ActionResult deliverySuchargePage(AdminPanelModel adminmodel)
        {

            return View("deliverySurchargePage", adminmodel);

        }

        public ActionResult getDeliverySurchargeFrame(AdminPanelModel adminmodel)
        {

            return View("deliverySurchargeFrame");


        }

        public ActionResult discountSetupPage(AdminPanelModel adminmodel)
        {

            return View("discountSetupPage", adminmodel);

        }

        public ActionResult getDiscountSetupFrame(AdminPanelModel adminmodel)
        {

            return View("discountSetupFrame");


        }



        public ActionResult itemsPage(String itemcode)
        {

            return View("itemsPage");

        }

       

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return PartialView("About");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return PartialView("Contact");
        }

      /*
        public ActionResult mainPage()
        {
            MenuRetievalModel model = new MenuRetievalModel();
            model.ABN = ABN;
            model.alertmessage = "";
            Session["model"] = model;
            ViewBag.ErrorMessage = "";
            model.getCategories();
            return View("mainPage", model);
        }
        */



        public ActionResult mainPageSession()
        {
            MenuRetievalModel model = (MenuRetievalModel)Session["model"];
            model.getCategories();
            return View("mainPage", model);
        }

        public ActionResult getDeliveryItem()
        {


            MenuRetievalModel model = (MenuRetievalModel)Session["model"];

            if (!model.getDeliveryOn())
            {
                return View("deliveryOffline");
            }

            model.getCategories();
            return PartialView("getMenuOrder", model);



        }



        public ActionResult getMenuCatItem()
        {

            MenuRetievalModel model = (MenuRetievalModel)Session["model"];

            return PartialView("getMenuOrder", (MenuRetievalModel)Session["model"]);

        }


        [HttpPost]
        public ActionResult createOrder(MenuRetievalModel model)
        {

            ((MenuRetievalModel)Session["model"]).name = model.name;
            ((MenuRetievalModel)Session["model"]).phone = model.phone;
            ((MenuRetievalModel)Session["model"]).address = model.address;
            ((MenuRetievalModel)Session["model"]).itemno = model.itemno; //suburb


            Session["orderdate"] = ((MenuRetievalModel)Session["model"]).createOrder("homedelivery");

            return PartialView("getMenuOrder", (MenuRetievalModel)Session["model"]);
        }

        [HttpPost]
        public ActionResult createTakeAway(MenuRetievalModel model)
        {


            ((MenuRetievalModel)Session["model"]).name = model.name;
            ((MenuRetievalModel)Session["model"]).phone = model.phone;

            Session["orderdate"] = ((MenuRetievalModel)Session["model"]).createOrderTA("takeaway");


            return PartialView("getMenuOrder", (MenuRetievalModel)Session["model"]);
        }

        public ActionResult updateQty(String itemno, String func)
        {

            ItemModel found = new ItemModel();
            MenuRetievalModel model = (MenuRetievalModel)Session["model"];
            
                found.itemno = itemno;
                int signal = 0;
                if (func == "add")
                {
                   
                    signal = 1;

                }
                else if (func == "deduct")
                {
                  
                    signal = -1;

                }
                model.transferItems(found, signal);
                model.recallDetails();

                return PartialView("paymentdialog", model);


        }

        public ActionResult refreshModel()
        {
            if (Session["model"] != null)
            {
                MenuRetievalModel model = (MenuRetievalModel)Session["model"];
                model.recallDetails();

            }

            return getMenuCatItem();
        }

        public void updateItemCode(String itemcode)
        {

            if (Session["model"] != null)
            {

                MenuRetievalModel model = (MenuRetievalModel)Session["model"];

                model.itemno = itemcode;


            }


        }


        [HttpPost]
        public ActionResult pay(MenuRetievalModel model)
        {
            model.ABN = ((MenuRetievalModel)Session["model"]).ABN;
            Session["model"] = model;
            model.orderdate = (DateTime)Session["orderdate"];
            model.pay("enter here");

            if (model.approved == "APPROVED")
            {
                if (((MenuRetievalModel)Session["model"]).OrderType == "takeaway")
                {
                    return PartialView("paid", model);
                }
                else
                {
                    return PartialView("paiddelivery", model);

                }
            }
            else
            {
                model.cardnumber = "";
                model.cardexpiry = "";
                model.cardcvv = "";
                return PartialView("declined", model);

            }

        }

        public ActionResult approved()
        {
            if (((MenuRetievalModel)Session["model"]).OrderType == "takeaway")
            {
                return PartialView("paid");
            }
            else
            {
                return PartialView("paiddelivery");

            }
        }

        public ActionResult declined()
        {

            return PartialView("declined");
        }

        public ActionResult expired()
        {

            return PartialView("expiredSession");

        }

        public ActionResult getShoppingCart()
        {

            return PartialView("ViewShoppingCart", (MenuRetievalModel)Session["model"]);

        }

        [HttpPost]
        public ActionResult forceModify(MenuRetievalModel model)
        {

            model.name = ((MenuRetievalModel)Session["model"]).name;
            model.phone = ((MenuRetievalModel)Session["model"]).phone;
            model.address = ((MenuRetievalModel)Session["model"]).address;
            model.itemno = ((MenuRetievalModel)Session["model"]).itemno; //suburb
            model.actualitemno = ((MenuRetievalModel)Session["model"]).itemno; //suburb
            model.Categories = ((MenuRetievalModel)Session["model"]).Categories;
            model.orderdate = (DateTime)Session["orderdate"];
            model.OrderType = ((MenuRetievalModel)Session["model"]).OrderType;
            model.ABN = ((MenuRetievalModel)Session["model"]).ABN;
            model.DataSetModifiers = ((MenuRetievalModel)Session["model"]).DataSetModifiers;
            Session["model"] = model;
            ModifierModel modon = model.GetNameOfModifierOn();
            model.forceModify(modon.BtnDesc, Convert.ToDecimal(modon.price));

            model.recallDetails();
            
            return PartialView("paymentdialog", (MenuRetievalModel)Session["model"]);
        }

        [HttpGet]
        public ActionResult getForceModfier(String itemno)
        {
            ((MenuRetievalModel)Session["model"]).itemno = itemno;
            ((MenuRetievalModel)Session["model"]).actualitemno = itemno;
            return PartialView("forceModify", (MenuRetievalModel)Session["model"]);

        }

        [HttpGet]
        public ActionResult getModifiers(String itemno, String actualitemno)
        {
            ((MenuRetievalModel)Session["model"]).itemno = itemno;
            ((MenuRetievalModel)Session["model"]).actualitemno = actualitemno;
            return PartialView("modify", (MenuRetievalModel)Session["model"]);

        }

        [HttpPost]
        public ActionResult modify(MenuRetievalModel model)
        {

            model.name = ((MenuRetievalModel)Session["model"]).name;
            model.phone = ((MenuRetievalModel)Session["model"]).phone;
            model.address = ((MenuRetievalModel)Session["model"]).address;
            model.Categories = ((MenuRetievalModel)Session["model"]).Categories;
            model.orderdate = (DateTime)Session["orderdate"];
            model.OrderType = ((MenuRetievalModel)Session["model"]).OrderType;
            model.ABN = ((MenuRetievalModel)Session["model"]).ABN;
            model.DataSetModifiers = ((MenuRetievalModel)Session["model"]).DataSetModifiers;
            Session["model"] = model;

            ModifierModel modon = model.GetNameOfModifierOn();
            model.forceModify(modon.BtnDesc, Convert.ToDecimal(modon.price));

            model.recallDetails();
            
            return PartialView("paymentdialog", (MenuRetievalModel)Session["model"]);
        }


    }
}
