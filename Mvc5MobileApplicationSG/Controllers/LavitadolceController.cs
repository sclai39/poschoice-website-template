﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc5MobileApplicationSG.Models;
using Stripe;

namespace Mvc5MobileApplicationSG.Controllers
{
    public class LavitadolceController : Controller
    {

        public static string ABN = "65483704728";
        //  protected static string acctid = "acct_1H1m8RHiZX8NuuLN";

        protected static string acctid = "acct_1H1ModHiCRtkZvz2";


        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return PartialView("About");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return PartialView("Contact");
        }

        public ActionResult mainPage()
        {

            MenuRetievalModel model = new MenuRetievalModel();
            model.ABN = ABN;
            model.alertmessage = "";
            Session["model"] = model;
            ViewBag.ErrorMessage = "";
            model.getCategories();
            return View("mainPage", model);


        }

        public ActionResult mainPageSession()
        {
            MenuRetievalModel model = (MenuRetievalModel)Session["model"];
            model.getCategories();
            return View("mainPage", model);
        }

        public ActionResult getDeliveryItem()
        {


            MenuRetievalModel model = (MenuRetievalModel)Session["model"];

            if (!model.getDeliveryOn())
            {
                return View("deliveryOffline");
            }

            model.getCategories();
            return PartialView("getMenuOrder", model);



        }

        [HttpGet]
        public ActionResult validateContact(String email, String cashcard)
        {


            ((MenuRetievalModel)Session["model"]).email = email;
            ((MenuRetievalModel)Session["model"]).Notes = cashcard;

            Session["id"] = ((MenuRetievalModel)Session["model"]).generateID();
            ((MenuRetievalModel)Session["model"]).updateContact();

            ViewBag.Invalid = "";

            return PartialView("validateContact", (MenuRetievalModel)Session["model"]);


        }

        [HttpGet]
        public ActionResult validateContactBooking(MenuRetievalModel model)
        {

            ((MenuRetievalModel)Session["model"]).BookingTable = model.BookingTable;
            ((MenuRetievalModel)Session["model"]).email = model.email;
            ((MenuRetievalModel)Session["model"]).OrderType = "booking";
            createBooking((MenuRetievalModel)Session["model"]);
            Session["id"] = ((MenuRetievalModel)Session["model"]).generateID();
            ((MenuRetievalModel)Session["model"]).updateContact();


            ViewBag.Invalid = "";
            ((MenuRetievalModel)Session["model"]).ProgressToBookingConfirmation = true;
            return PartialView("validateContact", (MenuRetievalModel)Session["model"]);


        }

        [HttpGet]
        public ActionResult validateBooking(String id)
        {

            if (id == Session["id"].ToString())
            {

                ((MenuRetievalModel)Session["model"]).confirmedcontact();
                return PartialView("confirmedbooking", (MenuRetievalModel)Session["model"]);


            }
            else
            {
                ViewBag.Invalid = "Invalid Pin, Please try again";
                return PartialView("validateContact", (MenuRetievalModel)Session["model"]);


            }



        }

        [HttpGet]
        public ActionResult validateDetails(String id, String ordertype)
        {

            if (id == Session["id"].ToString())
            {
                if (ordertype == "homedelivery")
                {
                    return PartialView("delivery", (MenuRetievalModel)Session["model"]);

                }
                else if (ordertype == "takeaway")
                {

                    return PartialView("takeawaydialog", (MenuRetievalModel)Session["model"]);

                }
                else
                {

                    return PartialView("bookingdialog", (MenuRetievalModel)Session["model"]);

                }
            }
            else
            {
                ViewBag.Invalid = "Invalid Pin, Please try again";
                return PartialView("validateContact", (MenuRetievalModel)Session["model"]);


            }



        }



        [HttpGet]
        public ActionResult validate(String id)
        {

            if (id == Session["id"].ToString())
            {
                ((MenuRetievalModel)Session["model"]).confirmedcontact();
                return PartialView("paid", (MenuRetievalModel)Session["model"]);


            }
            else
            {
                ViewBag.Invalid = "Invalid Pin, Please try again";
                return PartialView("validateContact", (MenuRetievalModel)Session["model"]);


            }



        }


        [HttpGet]
        public ActionResult findContactBooking(String phone)
        {
            if (Session["model"] == null)
            {
                refreshsession();

            }

            ((MenuRetievalModel)Session["model"]).OrderType = "booking";

            ((MenuRetievalModel)Session["model"]).bookingrequest = true;

            ((MenuRetievalModel)Session["model"]).phonesearch = phone;

            ((MenuRetievalModel)Session["model"]).findcontactBooking(phone);

            if (((MenuRetievalModel)Session["model"]).email != null && ((MenuRetievalModel)Session["model"]).email.Trim() != "")
            {
                Session["id"] = ((MenuRetievalModel)Session["model"]).generateID();


                return PartialView("validateContact", (MenuRetievalModel)Session["model"]);

            }

            return PartialView("bookingdialog", ((MenuRetievalModel)Session["model"]));


        }

        [HttpGet]
        public ActionResult findContactTA(String phone)
        {
            if (Session["model"] == null)
            {
                refreshsession();

            }

            ((MenuRetievalModel)Session["model"]).OrderType = "takeaway";

            ((MenuRetievalModel)Session["model"]).phonesearch = phone;

            ((MenuRetievalModel)Session["model"]).findcontactTA(phone, "takeaway");

            if (((MenuRetievalModel)Session["model"]).email != null && ((MenuRetievalModel)Session["model"]).email.Trim() != "")
            {
                Session["id"] = ((MenuRetievalModel)Session["model"]).generateID();


                return PartialView("validateContact", (MenuRetievalModel)Session["model"]);

            }

            return PartialView("takeawaydialog", ((MenuRetievalModel)Session["model"]));


        }


        [HttpGet]
        public ActionResult findContactDelivery(String phone)
        {
            if (Session["model"] == null)
            {
                refreshsession();

            }

            ((MenuRetievalModel)Session["model"]).OrderType = "homedelivery";

            ((MenuRetievalModel)Session["model"]).phonesearch = phone;

            ((MenuRetievalModel)Session["model"]).findcontact(phone, "homedelivery");

            if (((MenuRetievalModel)Session["model"]).email != null && ((MenuRetievalModel)Session["model"]).email.Trim() != "")
            {
                Session["id"] = ((MenuRetievalModel)Session["model"]).generateID();


                return PartialView("validateContact", (MenuRetievalModel)Session["model"]);

            }

            return PartialView("delivery", ((MenuRetievalModel)Session["model"]));


        }


        public ActionResult getMenuCatItem()
        {

            if (Session["model"] == null)
            {
                refreshsession();

            }
            MenuRetievalModel model = (MenuRetievalModel)Session["model"];

            return PartialView("getMenuOrder", (MenuRetievalModel)Session["model"]);

        }


        [HttpGet]
        public ActionResult getSearchDialog(String ordertype)
        {
            if (Session["model"] == null)
            {
                refreshsession();

            }


            ((MenuRetievalModel)Session["model"]).OrderType = ordertype;

            return PartialView("search", (MenuRetievalModel)Session["model"]);


        }


        [HttpGet]
        public ActionResult getSearchDialogBooking()
        {
            if (Session["model"] == null)
            {
                refreshsession();

            }


            ((MenuRetievalModel)Session["model"]).OrderType = "booking";

            return PartialView("search", (MenuRetievalModel)Session["model"]);


        }



        [HttpGet]
        public ActionResult getBookingDialog()
        {
            if (Session["model"] == null)
            {
                refreshsession();

            }

            ((MenuRetievalModel)Session["model"]).ProgressToBookingTable = false;
            ((MenuRetievalModel)Session["model"]).ProgressToBookingConfirmation = false;
            return PartialView("bookingdialog", (MenuRetievalModel)Session["model"]);


        }

        [HttpGet]
        public ActionResult getDeliveryDialog()
        {
            if (Session["model"] == null)
            {
                refreshsession();

            }

            ((MenuRetievalModel)Session["model"]).confirmDetails = false;
            ((MenuRetievalModel)Session["model"]).noDetails = true;
            ((MenuRetievalModel)Session["model"]).ProgressToBookingConfirmation = false;
            return PartialView("delivery", (MenuRetievalModel)Session["model"]);


        }

        public void refreshsession()
        {

            Session["model"] = new MenuRetievalModel();
            ((MenuRetievalModel)Session["model"]).ABN = ABN;
            ((MenuRetievalModel)Session["model"]).getCategories();

        }


        [HttpPost]
        public ActionResult taken(String name, String phone, String notes, String year, String month, String day, String fromhh, String frommm, String fromtt, String tohh, String tomm, String tott)
        {
            if (Session["model"] == null)
            {
                refreshsession();

            }

            ((MenuRetievalModel)Session["model"]).name = name;
            ((MenuRetievalModel)Session["model"]).phone = phone;
            ((MenuRetievalModel)Session["model"]).Notes = notes;

            ((MenuRetievalModel)Session["model"]).BookingYear = year;
            ((MenuRetievalModel)Session["model"]).BookingMonth = month;
            ((MenuRetievalModel)Session["model"]).BookingDay = day;

            ((MenuRetievalModel)Session["model"]).Fromhh = fromhh;
            ((MenuRetievalModel)Session["model"]).Frommm = frommm;
            ((MenuRetievalModel)Session["model"]).Fromtt = fromtt;

            ((MenuRetievalModel)Session["model"]).Tohh = tohh;
            ((MenuRetievalModel)Session["model"]).Tomm = tomm;
            ((MenuRetievalModel)Session["model"]).Tott = tott;


            /*
                        if (fromhh < 0 || fromhh > 24)
                        {
                            throw new Exception("Invalid Hrs");
                        }

                        if (tohh < 0 || tohh > 24)
                        {
                            throw new Exception("Invalid Hrs");
                        }

                        if (frommm < 0 || frommm > 60)
                        {
                            throw new Exception("Invalid Minutes");
                        }

                        if (tomm < 0 || tomm > 60)
                        {
                            throw new Exception("Invalid Minutes");
                        }


                        if (tohh < fromhh && tomm <= frommm && fromtt == tott && fromtt == "PM")
                        {
                            throw new Exception("from time must be greater than to time");
                        }

                        */

            ((MenuRetievalModel)Session["model"]).ProgressToBookingTable = true;
            return PartialView("bookingtable", (MenuRetievalModel)Session["model"]);


        }


        [HttpGet]
        public ActionResult bookingTable()
        {

            ((MenuRetievalModel)Session["model"]).ProgressToBookingTable = true;
            ((MenuRetievalModel)Session["model"]).ProgressToBookingConfirmation = false;
            return PartialView("bookingtable", (MenuRetievalModel)Session["model"]);


        }





        public void createBooking(MenuRetievalModel model)
        {

            ((MenuRetievalModel)Session["model"]).BookingTable = model.BookingTable.Trim();


            String year = ((MenuRetievalModel)Session["model"]).BookingYear;
            String month = ((MenuRetievalModel)Session["model"]).BookingMonth;
            String day = ((MenuRetievalModel)Session["model"]).BookingDay;

            String fromhh = ((MenuRetievalModel)Session["model"]).Fromhh;
            String frommm = ((MenuRetievalModel)Session["model"]).Frommm;
            String fromtt = ((MenuRetievalModel)Session["model"]).Fromtt;

            String tohh = ((MenuRetievalModel)Session["model"]).Tohh;
            String tomm = ((MenuRetievalModel)Session["model"]).Tomm;
            String tott = ((MenuRetievalModel)Session["model"]).Tott;


            /*
                        if (fromhh < 0 || fromhh > 24)
                        {
                            throw new Exception("Invalid Hrs");
                        }

                        if (tohh < 0 || tohh > 24)
                        {
                            throw new Exception("Invalid Hrs");
                        }

                        if (frommm < 0 || frommm > 60)
                        {
                            throw new Exception("Invalid Minutes");
                        }

                        if (tomm < 0 || tomm > 60)
                        {
                            throw new Exception("Invalid Minutes");
                        }


                        if (tohh < fromhh && tomm <= frommm && fromtt == tott && fromtt == "PM")
                        {
                            throw new Exception("from time must be greater than to time");
                        }

                        */

            int fromhr = 0;
            if (fromhh == "PM")
            {
                fromhr = Convert.ToInt32(fromhh) + 12;
            }
            else
            {

                fromhr = Convert.ToInt32(fromhh);


            }


            DateTime from = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day), fromhr, Convert.ToInt32(frommm), 0);



            int tohr = 0;
            if (tott == "PM")
            {
                tohr = Convert.ToInt32(tohh) + 12;
            }
            else
            {

                tohr = Convert.ToInt32(tohh);


            }

            DateTime to = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day), tohr, Convert.ToInt32(tomm), 0);


            ((MenuRetievalModel)Session["model"]).createBooking(from, to);




        }

        [HttpPost]
        public ActionResult createOrderNonModel(String name, String phone, String address, String itemno)
        {
            if (Session["model"] == null)
            {
                refreshsession();


            }

            ((MenuRetievalModel)Session["model"]).name = name;
            ((MenuRetievalModel)Session["model"]).phone = phone;
            ((MenuRetievalModel)Session["model"]).address = address;
            ((MenuRetievalModel)Session["model"]).Notes = "";
            if (itemno != null)
            {
                ((MenuRetievalModel)Session["model"]).itemno = itemno; //suburb 

            }

            if (((MenuRetievalModel)Session["model"]).email == null || ((MenuRetievalModel)Session["model"]).email.Trim() == "")
            {

                Session["orderdate"] = ((MenuRetievalModel)Session["model"]).createOrder("homedelivery");


            }
            else
            {
                Session["orderdate"] = ((MenuRetievalModel)Session["model"]).createOrderWithEmail("homedelivery");


            }

            return PartialView("getMenuOrder", (MenuRetievalModel)Session["model"]);
        }


        [HttpPost]
        public ActionResult createOrder(MenuRetievalModel model)
        {
            if (Session["model"] == null)
            {
                refreshsession();


            }

            ((MenuRetievalModel)Session["model"]).name = model.name;
            ((MenuRetievalModel)Session["model"]).phone = model.phone;
            ((MenuRetievalModel)Session["model"]).address = model.address;
            ((MenuRetievalModel)Session["model"]).Notes = model.Notes;
            ((MenuRetievalModel)Session["model"]).itemno = model.itemno; //suburb


            Session["orderdate"] = ((MenuRetievalModel)Session["model"]).createOrder("homedelivery");

            return PartialView("getMenuOrder", (MenuRetievalModel)Session["model"]);
        }

        [HttpPost]
        public ActionResult createOrderNonModelTA(String name, String phone)
        {

            if (Session["model"] == null)
            {

                refreshsession();


            }

            ((MenuRetievalModel)Session["model"]).name = name;
            ((MenuRetievalModel)Session["model"]).phone = phone;

            Session["orderdate"] = ((MenuRetievalModel)Session["model"]).createOrderTA("takeaway");


            return PartialView("getMenuOrder", (MenuRetievalModel)Session["model"]);
        }


        [HttpPost]
        public ActionResult createTakeAway(MenuRetievalModel model)
        {

            if (Session["model"] == null)
            {

                refreshsession();


            }

            ((MenuRetievalModel)Session["model"]).name = model.name;
            ((MenuRetievalModel)Session["model"]).phone = model.phone;
            ((MenuRetievalModel)Session["model"]).Notes = model.Notes;

            Session["orderdate"] = ((MenuRetievalModel)Session["model"]).createOrderTA("takeaway");


            return PartialView("getMenuOrder", (MenuRetievalModel)Session["model"]);
        }


        public ActionResult updateQty(String itemno, String func)
        {

            ItemModel found = new ItemModel();
            MenuRetievalModel model = (MenuRetievalModel)Session["model"];


            found.itemno = itemno;
            int signal = 0;
            if (func == "add")
            {

                signal = 1;

            }
            else if (func == "deduct")
            {

                signal = -1;

            }
            model.transferItems(found, signal);

            model.recallDetails();



            return PartialView("paymentdialog", model);


        }

        public ActionResult refreshModel()
        {
            if (Session["model"] != null)
            {
                MenuRetievalModel model = (MenuRetievalModel)Session["model"];
                model.recallDetails();

            }

            return getMenuCatItem();
        }

        public void updateItemCode(String itemcode)
        {

            if (Session["model"] != null)
            {

                MenuRetievalModel model = (MenuRetievalModel)Session["model"];

                model.itemno = itemcode;


            }


        }


        [HttpGet]
        public JsonResult pay(String amount)
        {

            MenuRetievalModel model = (MenuRetievalModel)Session["model"];
            model.ABN = ABN;
            int amt = Convert.ToInt32((Convert.ToDouble(amount) * 100));
            PaymentIntent res = model.payIntent(amt);

            ViewData["ClientSecret"] = res.ClientSecret;

            return Json(new { clientsecret = res.ClientSecret }, JsonRequestBehavior.AllowGet);


        }

        [HttpGet]
        public ActionResult payment(String amount)
        {
            MenuRetievalModel model = (MenuRetievalModel)Session["model"];

            model.ABN = ABN;

            model.orderdate = (DateTime)Session["orderdate"];

            DateTime ready = DateTime.Now;
            if (model.OrderType == "takeaway")
            {
                ready = ready.AddMinutes(20);

            }
            else
            {
                ready = ready.AddMinutes(50);
            }

            model.payWithReady(amount, ready);

            return PartialView("paymentdialog", model);


        }



        [HttpGet]
        public ActionResult back()
        {
            MenuRetievalModel model = (MenuRetievalModel)Session["model"];

            return PartialView("paymentdialog", model);


        }


        [HttpGet]
        public ActionResult backBooking()
        {
            MenuRetievalModel model = (MenuRetievalModel)Session["model"];

            return PartialView("bookingdialog", model);


        }





        public ActionResult approved()
        {
            if (((MenuRetievalModel)Session["model"]).OrderType == "takeaway")
            {
                return PartialView("paid");
            }
            else
            {
                return PartialView("paiddelivery");

            }
        }

        public ActionResult declined()
        {

            return PartialView("declined");
        }

        public ActionResult expired()
        {

            return PartialView("expiredSession");

        }

        public ActionResult getShoppingCart()
        {

            return PartialView("ViewShoppingCart", (MenuRetievalModel)Session["model"]);

        }

        [HttpPost]
        public ActionResult forceModify(MenuRetievalModel model)
        {

            model.name = ((MenuRetievalModel)Session["model"]).name;
            model.phone = ((MenuRetievalModel)Session["model"]).phone;
            model.address = ((MenuRetievalModel)Session["model"]).address;
            model.itemno = ((MenuRetievalModel)Session["model"]).itemno; //suburb
            model.actualitemno = ((MenuRetievalModel)Session["model"]).itemno; //suburb
            model.Categories = ((MenuRetievalModel)Session["model"]).Categories;
            model.orderdate = (DateTime)Session["orderdate"];
            model.OrderType = ((MenuRetievalModel)Session["model"]).OrderType;
            model.ABN = ((MenuRetievalModel)Session["model"]).ABN;
            model.DataSetModifiers = ((MenuRetievalModel)Session["model"]).DataSetModifiers;
            model.email = ((MenuRetievalModel)Session["model"]).email;
            Session["model"] = model;
            ModifierModel modon = model.GetNameOfModifierOn();
            model.forceModify(modon.BtnDesc, Convert.ToDecimal(modon.price));

            model.recallDetails();

            return PartialView("paymentdialog", (MenuRetievalModel)Session["model"]);
        }

        [HttpGet]
        public ActionResult getForceModfier(String itemno)
        {
            ((MenuRetievalModel)Session["model"]).itemno = itemno;
            ((MenuRetievalModel)Session["model"]).actualitemno = itemno;
            return PartialView("forceModify", (MenuRetievalModel)Session["model"]);

        }

        [HttpGet]
        public ActionResult getModifiers(String itemno, String actualitemno)
        {
            ((MenuRetievalModel)Session["model"]).itemno = itemno;
            ((MenuRetievalModel)Session["model"]).actualitemno = actualitemno;
            return PartialView("modify", (MenuRetievalModel)Session["model"]);

        }

        [HttpPost]
        public ActionResult modify(MenuRetievalModel model)
        {

            model.name = ((MenuRetievalModel)Session["model"]).name;
            model.phone = ((MenuRetievalModel)Session["model"]).phone;
            model.address = ((MenuRetievalModel)Session["model"]).address;
            model.Categories = ((MenuRetievalModel)Session["model"]).Categories;
            model.orderdate = (DateTime)Session["orderdate"];
            model.OrderType = ((MenuRetievalModel)Session["model"]).OrderType;
            model.email = ((MenuRetievalModel)Session["model"]).email;
            model.ABN = ABN;
            model.DataSetModifiers = ((MenuRetievalModel)Session["model"]).DataSetModifiers;
            Session["model"] = model;

            ModifierModel modon = model.GetNameOfModifierOn();
            model.forceModify(modon.BtnDesc, Convert.ToDecimal(modon.price));

            model.recallDetails();

            return PartialView("paymentdialog", (MenuRetievalModel)Session["model"]);
        }


    }
}
