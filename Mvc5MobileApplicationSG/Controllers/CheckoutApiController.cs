﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc5MobileApplicationSG.Models;
using Stripe;


namespace Mvc5MobileApplicationSG.Controllers
{
  
        public class CheckoutApiController : Controller
        {
            [HttpGet]
            public ActionResult Get()
            {
            MenuRetievalModel model = new MenuRetievalModel();
            var intent = model.payIntent();// ... Fetch or create the PaymentIntent
            return Json(new { client_secret = intent.ClientSecret });
            }
        }
    
}